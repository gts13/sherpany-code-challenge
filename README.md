# Sherpany Code Challenge

Few words before the main part of this code challenge. 

First of all, I would like to thank you for giving me the chance to do this challenge! I really enjoyed building this app 
although I was not able to do some bonus points, like search function or paging. For the latter I admit I have a little 
experience. Unfortunately, my schedule is really heavy and therefore my available time is limited. It sounds as an excuse, 
but it's a fact.

The whole implementation took me around 3 working days (8 hours each day) and I invested a lot of effort and time on the 
architecture of this app, since -in my opinion- architecture is the crucial part in software development. Having a good 
architecture help developers to easily grow the app by adding new features and make testing more convenient.

That's all! Enjoy! 

---

### The app is based on Clean architecture concepts
It consists of three layers: 
  
  **Data** which includes its own models, database, network clients, and repository. The repository pattern is used.
  
  **Domain** which includes its own models, and use cases of business logic. This layer orchestrates the flow of data from Data Layer to Presentation and the other way.
  
  **Presentation** which includes its own models. It's a single activity with multiple fragments built with MVVM pattern.

Usually before the implementation, I do a very first plan/architecture on my whiteboard. See the image "planning".

#### Android Jetpack Libraries
- Fragment
- ViewModel 
- Databinding
- Navigation component
- LiveData 
- Room
- Cardview
- Constraintlayout
- [DiffUtil](https://developer.android.com/reference/androidx/recyclerview/widget/DiffUtil)
- [ActivityScenario](https://developer.android.com/guide/components/activities/testing), instrumentation testing (part of AndroidX Test) 
- Espresso (UI tests)
- Mockito

#### Libraries
- [Koin](https://insert-koin.io/) for DI
- [Kotlin Coroutines](https://developer.android.com/kotlin/coroutines)
- [Coroutines Asynchronous Flow](https://kotlinlang.org/docs/reference/coroutines/flow.html#asynchronous-flow)
- [picasso](https://github.com/square/picasso), A powerful image downloading and caching library for Android
- [Retrofit](https://square.github.io/retrofit/)
- [OkHttp](https://square.github.io/okhttp/)
- [moshi](https://github.com/square/moshi), JSON library for Kotlin and Java 
- [Timber](https://github.com/JakeWharton/timber), a logger which provides utility on top of Android’s Log class

##### Notes
The app contains a very simple UI test (please turn off animations on your device or emulator before run it).
It also contains few unit tests for the presentation and data layers. Due to my limited time I didn't cover all the 
datasources and cases.

If you want to see more about my skills (like Rx, Hilt, Localisation, and more) then please have a look on my 
github projects which are used as experimental projects to try new technologies and libraries.
[Giannis Tsepas github](https://github.com/gs-ts)

Useful sources:
- [Clean Architecture Guide (with tested examples): Data Flow != Dependency Rule](https://proandroiddev.com/clean-architecture-data-flow-dependency-rule-615ffdd79e29)
