package com.sherpany.challenge

import android.app.Application

import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger

import com.sherpany.challenge.data.dataModule
import com.sherpany.challenge.domain.domainModule
import com.sherpany.challenge.presentation.presentationModule

import timber.log.Timber

class SherpanyApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        startKoin {
            // use AndroidLogger as Koin Logger - default Level.INFO
            androidLogger(Level.ERROR) //TODO: remove Level.ERROR
            // use the Android context given there
            androidContext(this@SherpanyApplication)
            // module list
            modules(listOf(dataModule, domainModule, presentationModule))
        }
    }
}
