package com.sherpany.challenge.domain.usecases

import kotlinx.coroutines.flow.Flow

import com.sherpany.challenge.domain.models.Post
import com.sherpany.challenge.domain.JsonPlaceholderRepository

class GetPostsUseCase(private val jsonPlaceholderRepository: JsonPlaceholderRepository) {

    operator fun invoke(): Flow<List<Post>> = jsonPlaceholderRepository.loadPostsAndUsersFromDatabase()
}
