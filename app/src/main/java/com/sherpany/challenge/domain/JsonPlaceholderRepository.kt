package com.sherpany.challenge.domain

import kotlinx.coroutines.flow.Flow

import com.sherpany.challenge.common.Result
import com.sherpany.challenge.domain.models.Post
import com.sherpany.challenge.domain.models.Album
import com.sherpany.challenge.domain.models.Photo

interface JsonPlaceholderRepository {

    fun loadPostsAndUsersFromDatabase(): Flow<List<Post>>

    suspend fun retrieveAndStorePostsAndUsers(): Result<Unit>

    suspend fun deletePost(postId: Int)

    suspend fun retrieveAndStoreAlbums(): Result<Unit>

    suspend fun loadAlbumsByUserId(userId: Int): List<Album>

    suspend fun retrieveAndStorePhotos(): Result<Unit>

    suspend fun loadPhotosByAlbumId(albumId: Int): List<Photo>
}
