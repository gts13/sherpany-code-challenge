package com.sherpany.challenge.domain.usecases

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

import com.sherpany.challenge.common.Result
import com.sherpany.challenge.domain.JsonPlaceholderRepository

/**
 *
 * Usecase which is executed only once at start up in order to
 * retrieve user and posts from network
 * and store them in database.
 *
 */
class DownloadPostsAndUsersUseCase(private val jsonPlaceholderRepository: JsonPlaceholderRepository) {

    suspend operator fun invoke(): Result<Unit> = withContext(Dispatchers.IO) {
        return@withContext jsonPlaceholderRepository.retrieveAndStorePostsAndUsers()
    }
}
