package com.sherpany.challenge.domain.usecases

import com.sherpany.challenge.domain.models.Photo
import com.sherpany.challenge.domain.JsonPlaceholderRepository

class GetPhotosByAlbumUseCase(private val jsonPlaceholderRepository: JsonPlaceholderRepository) {

    suspend operator fun invoke(albumId: Int): List<Photo> = jsonPlaceholderRepository.loadPhotosByAlbumId(albumId)
}
