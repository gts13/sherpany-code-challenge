package com.sherpany.challenge.domain.usecases

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

import com.sherpany.challenge.domain.JsonPlaceholderRepository

class DeletePostUseCase(private val jsonPlaceholderRepository: JsonPlaceholderRepository) {

    suspend operator fun invoke(postId: Int) = withContext(Dispatchers.IO) {
        return@withContext jsonPlaceholderRepository.deletePost(postId)
    }
}
