package com.sherpany.challenge.domain.usecases

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

import com.sherpany.challenge.common.Result
import com.sherpany.challenge.domain.JsonPlaceholderRepository

class DownloadPhotosUseCase(private val jsonPlaceholderRepository: JsonPlaceholderRepository) {

    suspend operator fun invoke(): Result<Unit> = withContext(Dispatchers.IO) {
        jsonPlaceholderRepository.retrieveAndStorePhotos()
    }
}
