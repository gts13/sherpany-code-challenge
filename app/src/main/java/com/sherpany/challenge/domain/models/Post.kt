package com.sherpany.challenge.domain.models

class Post(
    val id: Int,
    val title: String,
    val body: String,
    val userId: Int,
    val userEmail: String
)
