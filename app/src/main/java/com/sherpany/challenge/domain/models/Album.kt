package com.sherpany.challenge.domain.models

class Album(
    val id: Int,
    val title: String
)
