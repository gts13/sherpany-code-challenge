package com.sherpany.challenge.domain.usecases

import com.sherpany.challenge.domain.models.Album
import com.sherpany.challenge.domain.JsonPlaceholderRepository

class GetAlbumsByUserUseCase(private val jsonPlaceholderRepository: JsonPlaceholderRepository) {

    suspend operator fun invoke(userId: Int): List<Album> = jsonPlaceholderRepository.loadAlbumsByUserId(userId)
}
