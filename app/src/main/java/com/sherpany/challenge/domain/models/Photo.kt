package com.sherpany.challenge.domain.models

class Photo(
    val title: String,
    val url: String,
    val thumbnailUrl: String
)
