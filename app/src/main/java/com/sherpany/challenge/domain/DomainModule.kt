package com.sherpany.challenge.domain

import org.koin.dsl.module

import com.sherpany.challenge.domain.usecases.DeletePostUseCase
import com.sherpany.challenge.domain.usecases.DownloadAlbumsUseCase
import com.sherpany.challenge.domain.usecases.DownloadPhotosUseCase
import com.sherpany.challenge.domain.usecases.DownloadPostsAndUsersUseCase
import com.sherpany.challenge.domain.usecases.GetAlbumsByUserUseCase
import com.sherpany.challenge.domain.usecases.GetPhotosByAlbumUseCase
import com.sherpany.challenge.domain.usecases.GetPostsUseCase

val domainModule = module {

    factory { DownloadPostsAndUsersUseCase(jsonPlaceholderRepository = get()) }

    factory { DeletePostUseCase(jsonPlaceholderRepository = get()) }

    factory { GetPostsUseCase(jsonPlaceholderRepository = get()) }

    factory { DownloadAlbumsUseCase(jsonPlaceholderRepository = get()) }

    factory { GetAlbumsByUserUseCase(jsonPlaceholderRepository = get()) }

    factory { DownloadPhotosUseCase(jsonPlaceholderRepository = get()) }

    factory { GetPhotosByAlbumUseCase(jsonPlaceholderRepository = get()) }
}
