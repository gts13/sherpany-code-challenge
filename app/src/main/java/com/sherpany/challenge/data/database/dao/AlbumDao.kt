package com.sherpany.challenge.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.OnConflictStrategy

import com.sherpany.challenge.data.database.entities.AlbumEntity

@Dao
interface AlbumDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAlbums(posts: List<AlbumEntity>)

    @Query("SELECT * FROM albums WHERE user_id = :userId")
    suspend fun getAlbumsByUserId(userId: Int): List<AlbumEntity>
}
