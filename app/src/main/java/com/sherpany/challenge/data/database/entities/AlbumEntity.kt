package com.sherpany.challenge.data.database.entities

import androidx.room.Entity
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

import com.sherpany.challenge.domain.models.Album

@Entity(tableName = "albums")
class AlbumEntity(
    @ColumnInfo(name = "user_id") val userId: Int,
    @PrimaryKey val id: Int,
    val title: String
)

fun AlbumEntity.toDomainModel() = Album(
    id = id,
    title = title
)

fun List<AlbumEntity>.toDomainModel() = map { albumEntity ->
    albumEntity.toDomainModel()
}
