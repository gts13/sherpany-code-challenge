package com.sherpany.challenge.data.network.responses

import com.sherpany.challenge.data.database.entities.AddressEntity
import com.sherpany.challenge.data.database.entities.CompanyEntity
import com.sherpany.challenge.data.database.entities.GeoEntity
import com.sherpany.challenge.data.database.entities.UserEntity
import com.squareup.moshi.Json

data class UserResponse(
    @Json(name = "id")
    val id: Int,
    @Json(name = "name")
    val name: String,
    @Json(name = "username")
    val username: String,
    @Json(name = "email")
    val email: String,
    @Json(name = "address")
    val address: UserAddress,
    @Json(name = "phone")
    val phone: String,
    @Json(name = "website")
    val website: String,
    @Json(name = "company")
    val company: UserCompany
)

data class UserAddress(
    @Json(name = "street")
    val street: String,
    @Json(name = "suite")
    val suite: String,
    @Json(name = "city")
    val city: String,
    @Json(name = "zipcode")
    val zipcode: String,
    @Json(name = "geo")
    val geo: AddressGeo
)

data class AddressGeo(
    @Json(name = "lat")
    val lat: Double,
    @Json(name = "lng")
    val lng: Double
)

data class UserCompany(
    @Json(name = "name")
    val name: String,
    @Json(name = "catchPhrase")
    val catchPhrase: String,
    @Json(name = "bs")
    val bs: String
)

// map to DB entity: UserEntity
fun UserResponse.toDatabaseEntity() = UserEntity(
    id = id,
    name = name,
    username = username,
    email = email,
    address = AddressEntity(
        street = address.street,
        suite = address.suite,
        city = address.city,
        zipcode = address.zipcode,
        geo = GeoEntity(address.geo.lat, address.geo.lng)
    ),
    phone = phone,
    website = website,
    company = CompanyEntity(
        name = company.name,
        catchPhrase = company.catchPhrase,
        bs = company.bs
    )
)

