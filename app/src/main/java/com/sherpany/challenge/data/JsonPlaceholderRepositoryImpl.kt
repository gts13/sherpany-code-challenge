package com.sherpany.challenge.data

import java.lang.Exception

import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.Flow

import com.sherpany.challenge.common.Result
import com.sherpany.challenge.data.database.dao.PostDao
import com.sherpany.challenge.data.database.dao.UserDao
import com.sherpany.challenge.data.database.dao.AlbumDao
import com.sherpany.challenge.data.database.dao.PhotoDao
import com.sherpany.challenge.data.database.entities.relationships.toDomainModel
import com.sherpany.challenge.data.database.entities.toDomainModel
import com.sherpany.challenge.data.network.JsonPlaceholderClient
import com.sherpany.challenge.data.network.responses.AlbumResponse
import com.sherpany.challenge.data.network.responses.PhotoResponse
import com.sherpany.challenge.data.network.responses.PostResponse
import com.sherpany.challenge.data.network.responses.UserResponse
import com.sherpany.challenge.data.network.responses.toDatabaseEntity
import com.sherpany.challenge.domain.models.Post
import com.sherpany.challenge.domain.models.Album
import com.sherpany.challenge.domain.models.Photo
import com.sherpany.challenge.domain.JsonPlaceholderRepository

import timber.log.Timber

class JsonPlaceholderRepositoryImpl(
    private val jsonPlaceholderClient: JsonPlaceholderClient,
    private val postDao: PostDao,
    private val userDao: UserDao,
    private val albumDao: AlbumDao,
    private val photoDao: PhotoDao
) : JsonPlaceholderRepository {

    override suspend fun retrieveAndStorePostsAndUsers(): Result<Unit> {
        return try {
            return when (val responsePosts = jsonPlaceholderClient.fetchPosts()) {
                is Result.Success -> {
                    when (val responseUsers = jsonPlaceholderClient.fetchUsers()) {
                        is Result.Success -> {
                            storePostsInDatabase(responsePosts.data)
                            storeUsersInDatabase(responseUsers.data)
                            Result.Success(Unit)
                        }
                        is Result.Error -> handleError(responseUsers.exception, "retrieveAndStorePostsAndUsers")
                    }
                }
                is Result.Error -> handleError(responsePosts.exception, "retrieveAndStorePostsAndUsers")
            }
        } catch (e: Exception) {
            handleError(e, "retrieveAndStorePostsAndUsers")
        }
    }

    override fun loadPostsAndUsersFromDatabase(): Flow<List<Post>> {
        return postDao.getPostsAndUsers().map { postsAndUsers ->
            postsAndUsers.toDomainModel()
        }
    }

    override suspend fun deletePost(postId: Int) = postDao.deletePostById(postId)

    override suspend fun retrieveAndStoreAlbums(): Result<Unit> {
        return try {
            return when (val response = jsonPlaceholderClient.fetchAlbums()) {
                is Result.Success -> {
                    storeAlbumsInDatabase(response.data)
                    return Result.Success(Unit)
                }
                is Result.Error -> handleError(response.exception, "retrieveAndStoreAlbums")
            }
        } catch (e: Exception) {
            handleError(e, "retrieveAndStoreAlbums")
        }
    }

    override suspend fun loadAlbumsByUserId(userId: Int): List<Album> {
        return albumDao.getAlbumsByUserId(userId).map { albumEntities ->
            albumEntities.toDomainModel()
        }
    }

    override suspend fun retrieveAndStorePhotos(): Result<Unit> {
        return try {
            return when (val response = jsonPlaceholderClient.fetchPhotos()) {
                is Result.Success -> {
                    storePhotosInDatabase(response.data)
                    return Result.Success(Unit)
                }
                is Result.Error -> handleError(response.exception, "retrieveAndStorePhotos")
            }
        } catch (e: Exception) {
            handleError(e, "retrieveAndStorePhotos")
        }
    }

    override suspend fun loadPhotosByAlbumId(albumId: Int): List<Photo> {
        return photoDao.getPhotosByAlbumId(albumId).map { photoEntities ->
            photoEntities.toDomainModel()
        }
    }

    private suspend fun storePostsInDatabase(data: List<PostResponse>) {
        postDao.insertPosts(data.map { postResponse ->
            postResponse.toDatabaseEntity()
        })
    }

    private suspend fun storeUsersInDatabase(data: List<UserResponse>) {
        userDao.insertUsers(data.map { userResponse ->
            userResponse.toDatabaseEntity()
        })
    }

    private suspend fun storeAlbumsInDatabase(data: List<AlbumResponse>) {
        albumDao.insertAlbums(data.map { albumResponse ->
            albumResponse.toDatabaseEntity()
        })
    }

    private suspend fun storePhotosInDatabase(data: List<PhotoResponse>) {
        photoDao.insertPhotos(data.map { photoResponse ->
            photoResponse.toDatabaseEntity()
        })
    }

    private fun handleError(exception: Exception, methodName: String): Result.Error {
        Timber.e(exception, "$methodName error")
        return Result.Error(exception)
    }
}
