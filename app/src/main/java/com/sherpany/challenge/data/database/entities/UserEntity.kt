package com.sherpany.challenge.data.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Embedded
import androidx.room.ColumnInfo

@Entity(tableName = "users")
data class UserEntity(
    @PrimaryKey val id: Int,
    val name: String,
    val username: String,
    val email: String,
    @Embedded val address: AddressEntity,
    val phone: String,
    val website: String,
    @Embedded val company: CompanyEntity
)

data class AddressEntity(
    val street: String,
    val suite: String,
    val city: String,
    @ColumnInfo(name = "zip_code") val zipcode: String,
    @Embedded val geo: GeoEntity
)

data class GeoEntity(
    val lat: Double,
    val lng: Double
)

data class CompanyEntity(
    @ColumnInfo(name = "company_name") val name: String,
    val catchPhrase: String,
    val bs: String
)
