package com.sherpany.challenge.data.network.responses

import com.squareup.moshi.Json

import com.sherpany.challenge.data.database.entities.PostEntity

data class PostResponse(
    @Json(name = "userId")
    val userId: Int,
    @Json(name = "id")
    val id: Int,
    @Json(name = "title")
    val title: String,
    @Json(name = "body")
    val body: String
)

fun PostResponse.toDatabaseEntity() = PostEntity(
    userId = userId,
    id = id,
    title = title,
    body = body
)
