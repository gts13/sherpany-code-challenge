package com.sherpany.challenge.data.network.responses

import com.squareup.moshi.Json

import com.sherpany.challenge.data.database.entities.AlbumEntity

data class AlbumResponse(
    @Json(name = "userId")
    val userId: Int,
    @Json(name = "id")
    val id: Int,
    @Json(name = "title")
    val title: String
)

fun AlbumResponse.toDatabaseEntity() = AlbumEntity(
    userId = userId,
    id = id,
    title = title
)
