package com.sherpany.challenge.data.database.entities

import androidx.room.Entity
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

import com.sherpany.challenge.domain.models.Photo

@Entity(tableName = "photos")
class PhotoEntity(
    @ColumnInfo(name = "album_id") val albumId: Int,
    @PrimaryKey val id: Int,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)

fun PhotoEntity.toDomainModel() = Photo(
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl
)

fun List<PhotoEntity>.toDomainModel() = map { albumEntity ->
    albumEntity.toDomainModel()
}

