package com.sherpany.challenge.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.OnConflictStrategy

import com.sherpany.challenge.data.database.entities.PhotoEntity

@Dao
interface PhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPhotos(posts: List<PhotoEntity>)

    @Query("SELECT * FROM photos WHERE album_id = :albumId")
    suspend fun getPhotosByAlbumId(albumId: Int): List<PhotoEntity>
}
