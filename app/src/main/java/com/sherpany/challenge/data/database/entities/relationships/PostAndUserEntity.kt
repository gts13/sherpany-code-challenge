package com.sherpany.challenge.data.database.entities.relationships

import androidx.room.Embedded
import androidx.room.Relation

import com.sherpany.challenge.domain.models.Post
import com.sherpany.challenge.data.database.entities.UserEntity
import com.sherpany.challenge.data.database.entities.PostEntity

class PostAndUserEntity(
    @Embedded val postEntity: PostEntity,
    @Relation(
        parentColumn = "user_id",
        entityColumn = "id"
    )
    val userEntity: UserEntity?
)

fun PostAndUserEntity.toDomainModel() = Post(
    userId = postEntity.userId,
    id = postEntity.id,
    title = postEntity.title,
    body = postEntity.body,
    userEmail = userEntity?.email ?: "empty"
)

fun List<PostAndUserEntity>.toDomainModel() = map { postAndUserEntity ->
    postAndUserEntity.toDomainModel()
}
