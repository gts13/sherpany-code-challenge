package com.sherpany.challenge.data

import org.koin.dsl.module
import org.koin.android.ext.koin.androidApplication

import androidx.room.Room

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

import com.sherpany.challenge.data.database.AppDatabase
import com.sherpany.challenge.data.network.JsonPlaceholderApi
import com.sherpany.challenge.data.network.JsonPlaceholderClient
import com.sherpany.challenge.data.network.JsonPlaceholderClientImpl
import com.sherpany.challenge.domain.JsonPlaceholderRepository

val dataModule = module {

    single { provideJsonPlaceholderApi().create(JsonPlaceholderApi::class.java) }

    single<JsonPlaceholderClient> { JsonPlaceholderClientImpl(jsonPlaceholderApi = get()) }

    single { Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "posts-db").build() }

    single { get<AppDatabase>().postDao() }

    single { get<AppDatabase>().userDao() }

    single { get<AppDatabase>().albumDao() }

    single { get<AppDatabase>().photoDao() }

    single<JsonPlaceholderRepository> {
        JsonPlaceholderRepositoryImpl(
            jsonPlaceholderClient = get(),
            postDao = get(),
            userDao = get(),
            albumDao = get(),
            photoDao = get()
        )
    }
}

private val okHttpClient = OkHttpClient.Builder()
    .addInterceptor(run {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.apply {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        }
    })
    .build()

private val jsonMoshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private fun provideJsonPlaceholderApi(): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(jsonMoshi))
        .baseUrl("https://jsonplaceholder.typicode.com/")
        .client(okHttpClient)
        .build()
}
