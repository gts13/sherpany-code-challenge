package com.sherpany.challenge.data.database

import androidx.room.Database
import androidx.room.RoomDatabase

import com.sherpany.challenge.data.database.dao.PostDao
import com.sherpany.challenge.data.database.dao.UserDao
import com.sherpany.challenge.data.database.dao.AlbumDao
import com.sherpany.challenge.data.database.dao.PhotoDao
import com.sherpany.challenge.data.database.entities.PostEntity
import com.sherpany.challenge.data.database.entities.UserEntity
import com.sherpany.challenge.data.database.entities.AlbumEntity
import com.sherpany.challenge.data.database.entities.PhotoEntity

@Database(
    entities = [
        PostEntity::class,
        UserEntity::class,
        AlbumEntity::class,
        PhotoEntity::class
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun postDao(): PostDao

    abstract fun userDao(): UserDao

    abstract fun albumDao(): AlbumDao

    abstract fun photoDao(): PhotoDao
}
