package com.sherpany.challenge.data.network

import com.sherpany.challenge.common.Result
import com.sherpany.challenge.data.network.responses.PostResponse
import com.sherpany.challenge.data.network.responses.UserResponse
import com.sherpany.challenge.data.network.responses.AlbumResponse
import com.sherpany.challenge.data.network.responses.PhotoResponse

interface JsonPlaceholderClient {

    suspend fun fetchPosts(): Result<List<PostResponse>>

    suspend fun fetchUsers(): Result<List<UserResponse>>

    suspend fun fetchAlbums(): Result<List<AlbumResponse>>

    suspend fun fetchPhotos(): Result<List<PhotoResponse>>
}
