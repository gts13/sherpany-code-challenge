package com.sherpany.challenge.data.network

import retrofit2.http.GET
import retrofit2.Response

import com.sherpany.challenge.data.network.responses.PostResponse
import com.sherpany.challenge.data.network.responses.UserResponse
import com.sherpany.challenge.data.network.responses.AlbumResponse
import com.sherpany.challenge.data.network.responses.PhotoResponse

interface JsonPlaceholderApi {

    @GET("posts")
    suspend fun getPosts(): Response<List<PostResponse>>

    @GET("users")
    suspend fun getUsers(): Response<List<UserResponse>>

    @GET("albums")
    suspend fun getAlbums(): Response<List<AlbumResponse>>

    @GET("photos")
    suspend fun getPhotos(): Response<List<PhotoResponse>>
}
