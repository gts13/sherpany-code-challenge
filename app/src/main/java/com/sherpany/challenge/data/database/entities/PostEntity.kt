package com.sherpany.challenge.data.database.entities

import androidx.room.Entity
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

@Entity(tableName = "posts")
data class PostEntity(
    @ColumnInfo(name = "user_id") val userId: Int,
    @PrimaryKey val id: Int,
    val title: String,
    val body: String
)
