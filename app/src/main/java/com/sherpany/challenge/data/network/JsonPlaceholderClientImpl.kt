package com.sherpany.challenge.data.network

import java.io.IOException
import java.lang.Exception

import com.sherpany.challenge.common.Result
import com.sherpany.challenge.data.network.responses.PostResponse
import com.sherpany.challenge.data.network.responses.UserResponse
import com.sherpany.challenge.data.network.responses.AlbumResponse
import com.sherpany.challenge.data.network.responses.PhotoResponse

class JsonPlaceholderClientImpl(private val jsonPlaceholderApi: JsonPlaceholderApi) : JsonPlaceholderClient {

    override suspend fun fetchPosts(): Result<List<PostResponse>> {
        return try {
            val response = jsonPlaceholderApi.getPosts()
            if (response.isSuccessful) {
                response.body()?.let {
                    return Result.Success(it)
                } ?: Result.Error(IOException("getPosts null response body"))
            } else {
                Result.Error(IOException("getPosts response failed"))
            }
        } catch (e: Exception) {
            Result.Error(IOException("fetchPosts exception", e))
        }
    }

    override suspend fun fetchUsers(): Result<List<UserResponse>> {
        return try {
            val response = jsonPlaceholderApi.getUsers()
            if (response.isSuccessful) {
                response.body()?.let {
                    return Result.Success(it)
                } ?: Result.Error(IOException("getUsers null response body"))
            } else {
                Result.Error(IOException("getUsers response failed"))
            }
        } catch (e: Exception) {
            Result.Error(IOException("fetchUsers exception", e))
        }
    }

    override suspend fun fetchAlbums(): Result<List<AlbumResponse>> {
        return try {
            val response = jsonPlaceholderApi.getAlbums()
            if (response.isSuccessful) {
                response.body()?.let {
                    return Result.Success(it)
                } ?: Result.Error(IOException("getAlbums null response body"))
            } else {
                Result.Error(IOException("getAlbums response failed"))
            }
        } catch (e: Exception) {
            Result.Error(IOException("fetchAlbums exception", e))
        }
    }

    override suspend fun fetchPhotos(): Result<List<PhotoResponse>> {
        return try {
            val response = jsonPlaceholderApi.getPhotos()
            if (response.isSuccessful) {
                response.body()?.let {
                    return Result.Success(it)
                } ?: Result.Error(IOException("getPhotos null response body"))
            } else {
                Result.Error(IOException("getPhotos response failed"))
            }
        } catch (e: Exception) {
            Result.Error(IOException("fetchPhotos exception", e))
        }
    }
}
