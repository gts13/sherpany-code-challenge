package com.sherpany.challenge.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.OnConflictStrategy

import kotlinx.coroutines.flow.Flow

import com.sherpany.challenge.data.database.entities.PostEntity
import com.sherpany.challenge.data.database.entities.relationships.PostAndUserEntity

@Dao
interface PostDao {

    @Query("SELECT * FROM posts")
    fun getPosts(): Flow<List<PostEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPosts(posts: List<PostEntity>)

    @Query("DELETE FROM posts WHERE id = :id")
    suspend fun deletePostById(id: Int)

    @Transaction
    @Query("SELECT * FROM posts")
    fun getPostsAndUsers(): Flow<List<PostAndUserEntity>>
}
