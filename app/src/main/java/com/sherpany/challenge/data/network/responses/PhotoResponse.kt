package com.sherpany.challenge.data.network.responses

import com.squareup.moshi.Json

import com.sherpany.challenge.data.database.entities.PhotoEntity

class PhotoResponse(
    @Json(name = "albumId")
    val albumId: Int,
    @Json(name = "id")
    val id: Int,
    @Json(name = "title")
    val title: String,
    @Json(name = "url")
    val url: String,
    @Json(name = "thumbnailUrl")
    val thumbnailUrl: String
)

fun PhotoResponse.toDatabaseEntity() = PhotoEntity(
    albumId = albumId,
    id = id,
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl
)

