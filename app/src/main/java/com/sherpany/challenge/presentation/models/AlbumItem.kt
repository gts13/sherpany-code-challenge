package com.sherpany.challenge.presentation.models

import com.sherpany.challenge.domain.models.Album

data class AlbumItem(
    val id: Int,
    val title: String
)

fun List<Album>.toPresentationModel() = map { album ->
    album.toPresentationModel()
}

fun Album.toPresentationModel() = AlbumItem(
    id = id,
    title = title
)
