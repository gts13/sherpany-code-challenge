package com.sherpany.challenge.presentation

import org.koin.dsl.module
import org.koin.androidx.viewmodel.dsl.viewModel

import com.sherpany.challenge.presentation.screens.MainViewModel

val presentationModule = module {

    viewModel {
        MainViewModel(
            downloadPostsAndUsersUseCase = get(),
            getPostsUseCase = get(),
            deletePostUseCase = get(),
            downloadAlbumsUseCase = get(),
            getAlbumsByUserUseCase = get(),
            downloadPhotosUseCase = get(),
            getPhotosByAlbumUseCase = get()
        )
    }
}
