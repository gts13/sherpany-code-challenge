package com.sherpany.challenge.presentation.screens.postdetail

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

import org.koin.androidx.viewmodel.ext.android.sharedViewModel

import com.sherpany.challenge.R
import com.sherpany.challenge.presentation.utils.viewBinding
import com.sherpany.challenge.presentation.screens.MainViewModel
import com.sherpany.challenge.databinding.FragmentPostDetailBinding
import com.sherpany.challenge.presentation.models.AlbumItem

class PostDetailFragment : Fragment(R.layout.fragment_post_detail) {

    private val binding by viewBinding(FragmentPostDetailBinding::bind)
    private val viewModel: MainViewModel by sharedViewModel()
    private lateinit var albumAdapter: AlbumAdapter
    private var isPhotosDownloaded = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.albumRecyclerView.isVisible = isPhotosDownloaded
        if (!isPhotosDownloaded) {
            binding.albumsProgressBar.isVisible = true
            viewModel.downloadPhotos()
        }

        albumAdapter = AlbumAdapter(onAlbumClicked = ::onAlbumClicked)
        binding.albumRecyclerView.adapter = albumAdapter

        val userId = arguments?.getInt("userId") ?: 0

        with(binding) {
            titleTextView.text = arguments?.getString("title") ?: ""
            bodyTextView.text = arguments?.getString("body") ?: ""
        }

        viewModel.getUserAlbums(userId)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.albums.observe(viewLifecycleOwner, { albums ->
            albumAdapter.submitList(albums)
        })

        viewModel.photosDownloaded.observe(viewLifecycleOwner, { photosDownloaded ->
            isPhotosDownloaded = photosDownloaded
            binding.albumsProgressBar.isVisible = false
            binding.albumRecyclerView.isVisible = true
        })
    }

    private fun onAlbumClicked(albumItem: AlbumItem) {
        findNavController().navigate(
            R.id.action_postDetail_to_photos, bundleOf(
                "albumTitle" to albumItem.title,
                "albumId" to albumItem.id
            )
        )
    }
}
