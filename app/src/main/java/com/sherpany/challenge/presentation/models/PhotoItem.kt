package com.sherpany.challenge.presentation.models

import com.sherpany.challenge.domain.models.Photo

data class PhotoItem(
    val title: String,
    val url: String,
    val thumbnailUrl: String
)

fun List<Photo>.toPresentationModel() = map { photo ->
    photo.toPresentationModel()
}

fun Photo.toPresentationModel() = PhotoItem(
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl
)
