package com.sherpany.challenge.presentation.models

import com.sherpany.challenge.domain.models.Post

class PostItem(
    val id: Int,
    val title: String,
    val body: String,
    val userId: Int,
    val userEmail: String
)

fun List<Post>.toPresentationModel() = map { post ->
    post.toPresentationModel()
}

fun Post.toPresentationModel() = PostItem(
    id = id,
    title = title,
    body = body,
    userId = userId,
    userEmail = userEmail
)
