package com.sherpany.challenge.presentation.screens.photos

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sherpany.challenge.R

import com.squareup.picasso.Picasso

import com.sherpany.challenge.databinding.PhotoItemBinding
import com.sherpany.challenge.presentation.models.PhotoItem

class PhotoAdapter : ListAdapter<PhotoItem, PhotoAdapter.PhotoViewHolder>(DiffCallback()) {

    class DiffCallback : DiffUtil.ItemCallback<PhotoItem>() {
        override fun areItemsTheSame(oldItem: PhotoItem, newItem: PhotoItem) = oldItem.title == newItem.title

        override fun areContentsTheSame(oldItem: PhotoItem, newItem: PhotoItem) = oldItem == newItem
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PhotoViewHolder.from(parent)

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) =
        holder.bind(getItem(position))

    class PhotoViewHolder private constructor(private val binding: PhotoItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(parent: ViewGroup): PhotoViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = PhotoItemBinding.inflate(
                    layoutInflater,
                    parent,
                    false
                )
                return PhotoViewHolder(binding)
            }
        }

        fun bind(item: PhotoItem) {
            binding.photo = item
            Picasso.get().load(item.thumbnailUrl)
                .placeholder(R.drawable.downloading_animation)
                .fit()
                .noFade()
                .into(binding.photoImageView)
            binding.executePendingBindings()
        }
    }

}
