package com.sherpany.challenge.presentation.screens.posts

import android.view.View
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController

import org.koin.androidx.viewmodel.ext.android.viewModel

import com.sherpany.challenge.R
import com.sherpany.challenge.databinding.FragmentPostsBinding
import com.sherpany.challenge.presentation.screens.MainViewModel
import com.sherpany.challenge.presentation.models.PostItem
import com.sherpany.challenge.presentation.utils.viewBinding

class PostsFragment : Fragment(R.layout.fragment_posts) {

    private val binding by viewBinding(FragmentPostsBinding::bind)
    private val viewModel: MainViewModel by viewModel()
    private lateinit var postAdapter: PostAdapter
    private var isDataDownloaded = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isDataDownloaded = savedInstanceState?.getBoolean(IS_DATA_DOWNLOADED) ?: false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.postRecyclerView.isVisible = isDataDownloaded
        if (!isDataDownloaded) {
            binding.postsProgressBar.isVisible = true
            viewModel.downloadPostsAndUsers()
            viewModel.downloadAlbums()
        }

        postAdapter = PostAdapter(
            onPostClicked = ::onPostClicked,
            onPostDeleted = viewModel::deletePost
        )
        binding.postRecyclerView.adapter = postAdapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.postsAndUsersDownloaded.observe(viewLifecycleOwner, { dataDownloaded ->
            isDataDownloaded = dataDownloaded
            binding.postsProgressBar.isVisible = false
            binding.postRecyclerView.isVisible = true
        })

        viewModel.posts.observe(viewLifecycleOwner, { postItems ->
            postAdapter.submitList(postItems)
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putBoolean(IS_DATA_DOWNLOADED, isDataDownloaded)
        }
        super.onSaveInstanceState(outState)
    }

    private fun onPostClicked(item: PostItem) {
        findNavController().navigate(
            R.id.action_posts_to_postDetail, bundleOf(
                "title" to item.title,
                "body" to item.body,
                "userId" to item.userId
            )
        )
    }

    companion object {
        private const val IS_DATA_DOWNLOADED = "isDataDownloaded"
    }
}
