package com.sherpany.challenge.presentation.screens.posts

import android.view.ViewGroup
import android.view.LayoutInflater
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

import com.sherpany.challenge.databinding.PostItemBinding
import com.sherpany.challenge.presentation.models.PostItem

class PostAdapter(
    private val onPostClicked: (PostItem) -> Unit,
    private val onPostDeleted: (Int) -> Unit
) : ListAdapter<PostItem, PostAdapter.PostViewHolder>(DiffCallback()) {

    class DiffCallback : DiffUtil.ItemCallback<PostItem>() {
        override fun areItemsTheSame(oldItem: PostItem, newItem: PostItem) = oldItem.title == newItem.title

        override fun areContentsTheSame(oldItem: PostItem, newItem: PostItem) = oldItem == newItem
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PostViewHolder.from(parent)

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) =
        holder.bind(
            getItem(position),
            onPostClicked,
            onPostDeleted
        )

    class PostViewHolder private constructor(private val binding: PostItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(parent: ViewGroup): PostViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = PostItemBinding.inflate(
                    layoutInflater,
                    parent,
                    false
                )
                return PostViewHolder(binding)
            }
        }

        fun bind(
            item: PostItem,
            onPostClicked: (PostItem) -> Unit,
            onPostDeleted: (Int) -> Unit
        ) {
            binding.post = item
            binding.postLayout.setOnClickListener { onPostClicked(item) }
            binding.deleteImageView.setOnClickListener { onPostDeleted(item.id) }
            binding.executePendingBindings()
        }
    }
}
