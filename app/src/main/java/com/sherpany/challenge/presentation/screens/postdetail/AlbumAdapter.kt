package com.sherpany.challenge.presentation.screens.postdetail

import android.view.ViewGroup
import android.view.LayoutInflater
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

import com.sherpany.challenge.databinding.AlbumItemBinding
import com.sherpany.challenge.presentation.models.AlbumItem

class AlbumAdapter(
    private val onAlbumClicked: (AlbumItem) -> Unit
) : ListAdapter<AlbumItem, AlbumAdapter.AlbumViewHolder>(DiffCallback()) {

    class DiffCallback : DiffUtil.ItemCallback<AlbumItem>() {
        override fun areItemsTheSame(oldItem: AlbumItem, newItem: AlbumItem) = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: AlbumItem, newItem: AlbumItem) = oldItem == newItem
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = AlbumViewHolder.from(parent)

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) =
        holder.bind(
            getItem(position),
            onAlbumClicked
        )

    class AlbumViewHolder private constructor(private val binding: AlbumItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(parent: ViewGroup): AlbumViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = AlbumItemBinding.inflate(
                    layoutInflater,
                    parent,
                    false
                )
                return AlbumViewHolder(binding)
            }
        }

        fun bind(
            item: AlbumItem,
            onAlbumClicked: (AlbumItem) -> Unit
        ) {
            binding.album = item
            binding.albumCardView.setOnClickListener { onAlbumClicked(item) }
            binding.executePendingBindings()
        }
    }

}
