package com.sherpany.challenge.presentation.screens

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope

import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.map

import com.sherpany.challenge.common.Result
import com.sherpany.challenge.domain.usecases.DownloadPostsAndUsersUseCase
import com.sherpany.challenge.domain.usecases.DeletePostUseCase
import com.sherpany.challenge.domain.usecases.GetPostsUseCase
import com.sherpany.challenge.domain.usecases.DownloadAlbumsUseCase
import com.sherpany.challenge.domain.usecases.DownloadPhotosUseCase
import com.sherpany.challenge.domain.usecases.GetAlbumsByUserUseCase
import com.sherpany.challenge.domain.usecases.GetPhotosByAlbumUseCase
import com.sherpany.challenge.presentation.models.AlbumItem
import com.sherpany.challenge.presentation.models.PhotoItem
import com.sherpany.challenge.presentation.models.PostItem
import com.sherpany.challenge.presentation.models.toPresentationModel

import timber.log.Timber

class MainViewModel(
    private val downloadPostsAndUsersUseCase: DownloadPostsAndUsersUseCase,
    getPostsUseCase: GetPostsUseCase,
    private val deletePostUseCase: DeletePostUseCase,
    private val downloadAlbumsUseCase: DownloadAlbumsUseCase,
    private val getAlbumsByUserUseCase: GetAlbumsByUserUseCase,
    private val downloadPhotosUseCase: DownloadPhotosUseCase,
    private val getPhotosByAlbumUseCase: GetPhotosByAlbumUseCase
) : ViewModel() {

    private val _postsAndUsersDownloaded = MutableLiveData<Boolean>()
    val postsAndUsersDownloaded: LiveData<Boolean>
        get() = _postsAndUsersDownloaded

    private val _albums = MutableLiveData<List<AlbumItem>>()
    val albums: LiveData<List<AlbumItem>>
        get() = _albums

    private val _photosDownloaded = MutableLiveData<Boolean>()
    val photosDownloaded: LiveData<Boolean>
        get() = _photosDownloaded

    private val _photos = MutableLiveData<List<PhotoItem>>()
    val photos: LiveData<List<PhotoItem>>
        get() = _photos

    fun downloadPostsAndUsers() {
        viewModelScope.launch {
            when (val result = downloadPostsAndUsersUseCase.invoke()) {
                is Result.Success -> _postsAndUsersDownloaded.postValue(true)
                is Result.Error -> Timber.e(result.exception, "downloadPostsAndUsers error")
            }
        }
    }

    val posts: LiveData<List<PostItem>> = getPostsUseCase.invoke()
        .map { posts -> posts.toPresentationModel() }
        .asLiveData()

    fun deletePost(postId: Int) {
        viewModelScope.launch {
            deletePostUseCase.invoke(postId)
        }
    }

    fun downloadAlbums() {
        viewModelScope.launch {
            when (val result = downloadAlbumsUseCase.invoke()) {
                is Result.Success -> Timber.d("downloadAlbums success")
                is Result.Error -> Timber.e(result.exception, "downloadAlbums error")
            }
        }
    }

    fun getUserAlbums(userId: Int) {
        viewModelScope.launch {
            val albums = getAlbumsByUserUseCase.invoke(userId)
            _albums.postValue(albums.toPresentationModel())
        }
    }

    fun downloadPhotos() {
        viewModelScope.launch {
            when (downloadPhotosUseCase.invoke()) {
                is Result.Success -> _photosDownloaded.postValue(true)
                is Result.Error -> Timber.d("download photos fail")
            }
        }
    }

    fun getAlbumPhotos(albumId: Int) {
        viewModelScope.launch {
            val photos = getPhotosByAlbumUseCase.invoke(albumId)
            _photos.postValue(photos.toPresentationModel())
        }
    }
}
