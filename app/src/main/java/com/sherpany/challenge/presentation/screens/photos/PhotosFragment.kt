package com.sherpany.challenge.presentation.screens.photos

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment

import org.koin.androidx.viewmodel.ext.android.sharedViewModel

import com.sherpany.challenge.R
import com.sherpany.challenge.databinding.FragmentPhotosBinding
import com.sherpany.challenge.presentation.screens.MainViewModel
import com.sherpany.challenge.presentation.utils.viewBinding

class PhotosFragment : Fragment(R.layout.fragment_photos) {

    private val binding by viewBinding(FragmentPhotosBinding::bind)
    private val viewModel: MainViewModel by sharedViewModel()
    private lateinit var photoAdapter: PhotoAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val albumTitle = arguments?.getString("albumTitle") ?: ""
        val albumId = arguments?.getInt("albumId") ?: 0

        viewModel.getAlbumPhotos(albumId)

        photoAdapter = PhotoAdapter()
        binding.photoRecyclerView.adapter = photoAdapter
        binding.albumTitleTextView.text = albumTitle
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.photos.observe(viewLifecycleOwner, { photos ->
            photoAdapter.submitList(photos)
        })
    }
}
