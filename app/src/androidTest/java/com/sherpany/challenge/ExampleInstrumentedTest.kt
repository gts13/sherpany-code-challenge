package com.sherpany.challenge

import org.junit.Test
import org.junit.runner.RunWith

import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.recyclerview.widget.RecyclerView

import com.sherpany.challenge.utils.waitId
import com.sherpany.challenge.presentation.MainActivity

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    // device MUST have all animations OFF
    // check here: https://developer.android.com/training/testing/espresso/setup#set-up-environment
    @Test
    fun givenListsOfPosts_WhenClickPost_ThenShowNextScreenWithTitle() {
        ActivityScenario.launch(MainActivity::class.java)

        waitId(R.id.postsProgressBar, 5000)

        onView(withId(R.id.postRecyclerView))
            .perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        onView(withId(R.id.titleTextView)).check(
            ViewAssertions.matches(
                ViewMatchers.isDisplayed()
            )
        )
    }
}
