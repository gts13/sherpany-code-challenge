package com.sherpany.challenge.presentation

import org.junit.Test
import org.junit.Rule
import org.junit.Before
import org.junit.Assert

import androidx.arch.core.executor.testing.InstantTaskExecutorRule

import kotlinx.coroutines.test.setMain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.ExperimentalCoroutinesApi

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever

import com.sherpany.challenge.common.Result
import com.sherpany.challenge.domain.models.Album
import com.sherpany.challenge.domain.models.Photo
import com.sherpany.challenge.domain.usecases.DeletePostUseCase
import com.sherpany.challenge.domain.usecases.DownloadAlbumsUseCase
import com.sherpany.challenge.domain.usecases.DownloadPhotosUseCase
import com.sherpany.challenge.domain.usecases.DownloadPostsAndUsersUseCase
import com.sherpany.challenge.domain.usecases.GetAlbumsByUserUseCase
import com.sherpany.challenge.domain.usecases.GetPhotosByAlbumUseCase
import com.sherpany.challenge.domain.usecases.GetPostsUseCase
import com.sherpany.challenge.presentation.models.AlbumItem
import com.sherpany.challenge.presentation.models.PhotoItem
import com.sherpany.challenge.presentation.models.toPresentationModel
import com.sherpany.challenge.presentation.screens.MainViewModel

class MainViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: MainViewModel
    private val mockDownloadPostsAndUsersUseCase: DownloadPostsAndUsersUseCase = mock()
    private val mockGetPostsUseCase: GetPostsUseCase = mock()
    private val mockDeletePostUseCase: DeletePostUseCase = mock()
    private val mockDownloadAlbumsUseCase: DownloadAlbumsUseCase = mock()
    private val mockGetAlbumsByUserUseCase: GetAlbumsByUserUseCase = mock()
    private val mockDownloadPhotosUseCasee: DownloadPhotosUseCase = mock()
    private val mockGetPhotosByAlbumUseCase: GetPhotosByAlbumUseCase = mock()

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        viewModel = MainViewModel(
            mockDownloadPostsAndUsersUseCase,
            mockGetPostsUseCase,
            mockDeletePostUseCase,
            mockDownloadAlbumsUseCase,
            mockGetAlbumsByUserUseCase,
            mockDownloadPhotosUseCasee,
            mockGetPhotosByAlbumUseCase
        )
    }

    @Test
    fun `given successful network operation, when download posts and users, then return true`() {
        // given
        val expected = Result.Success(Unit)
        runBlocking { whenever(mockDownloadPostsAndUsersUseCase.invoke()).thenReturn(expected) }
        // when
        viewModel.downloadPostsAndUsers()
        // then
        val postsAndUsersDownloaded: Boolean? = LiveDataTestUtil.getValue(viewModel.postsAndUsersDownloaded)
        Assert.assertTrue(postsAndUsersDownloaded!!)
    }

    @Test
    fun `given successful network operation, when download photos, then return true`() {
        // given
        val expected = Result.Success(Unit)
        runBlocking { whenever(mockDownloadPhotosUseCasee.invoke()).thenReturn(expected) }
        // when
        viewModel.downloadPhotos()
        // then
        val photosDownloaded: Boolean? = LiveDataTestUtil.getValue(viewModel.photosDownloaded)
        Assert.assertTrue(photosDownloaded!!)
    }

    @Test
    fun `given albums in db, when get user albums, then return a list of album items`() {
        // given
        val expected = listOf<Album>()
        runBlocking { whenever(mockGetAlbumsByUserUseCase.invoke(any())).thenReturn(expected) }
        // when
        viewModel.getUserAlbums(any())
        // then
        val albumList: List<AlbumItem>? = LiveDataTestUtil.getValue(viewModel.albums)
        Assert.assertNotNull(albumList)
        Assert.assertEquals(expected.map { it.toPresentationModel() }, albumList)
    }

    @Test
    fun `given photos in db, when get album photos, then return a list of photo items`() {
        // given
        val expected = listOf<Photo>()
        runBlocking { whenever(mockGetPhotosByAlbumUseCase.invoke(any())).thenReturn(expected) }
        // when
        viewModel.getAlbumPhotos(any())
        // then
        val photoList: List<PhotoItem>? = LiveDataTestUtil.getValue(viewModel.photos)
        Assert.assertNotNull(photoList)
        Assert.assertEquals(expected.map { it.toPresentationModel() }, photoList)
    }
}
