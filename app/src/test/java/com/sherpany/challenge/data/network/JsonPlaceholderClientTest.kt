package com.sherpany.challenge.data.network

import org.junit.Test
import org.junit.Before

import kotlinx.coroutines.runBlocking

import com.nhaarman.mockitokotlin2.mock

import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert

import com.sherpany.challenge.data.network.utils.ApiAbstract

class JsonPlaceholderClientTest : ApiAbstract<JsonPlaceholderApi>() {

    private lateinit var jsonPlaceholderApi: JsonPlaceholderApi
    private val jsonPlaceholderClient: JsonPlaceholderClient = mock()

    @Before
    fun initService() {
        jsonPlaceholderApi = createService(JsonPlaceholderApi::class.java)
    }

    @Test
    fun `fetch albums from network`() = runBlocking {
        enqueueResponse(ALBUMS_RESPONSE)
        val response = jsonPlaceholderApi.getAlbums()
        val responseBody = response.body()!!
        mockWebServer.takeRequest()

        jsonPlaceholderClient.fetchAlbums()
        MatcherAssert.assertThat(responseBody.size, CoreMatchers.`is`(3))
        MatcherAssert.assertThat(responseBody[0].userId, CoreMatchers.`is`(1))
        MatcherAssert.assertThat(responseBody[0].id, CoreMatchers.`is`(1))
        MatcherAssert.assertThat(responseBody[0].title, CoreMatchers.`is`("quidem molestiae enim"))
    }

    companion object {
        private const val DIR_RESPONSES = "src/test/resources/responses"
        private const val ALBUMS_RESPONSE = "$DIR_RESPONSES/albumsResponse.json"
    }
}


