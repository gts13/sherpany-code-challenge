package com.sherpany.challenge.data

import org.junit.Test
import org.junit.Before
import org.junit.Assert

import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever

import com.sherpany.challenge.common.Result
import com.sherpany.challenge.data.network.JsonPlaceholderClient
import com.sherpany.challenge.data.database.dao.PostDao
import com.sherpany.challenge.data.database.dao.UserDao
import com.sherpany.challenge.data.database.dao.AlbumDao
import com.sherpany.challenge.data.database.dao.PhotoDao

class JsonPlaceholderRepositoryTest {

    private lateinit var repository: JsonPlaceholderRepositoryImpl

    private val mockJsonPlaceholderClient: JsonPlaceholderClient = mock()
    private val mockPostDao: PostDao = mock()
    private val mockUserDao: UserDao = mock()
    private val mockAlbumDao: AlbumDao = mock()
    private val mockPhotoDao: PhotoDao = mock()

    @Before
    fun setUp() {
        repository = JsonPlaceholderRepositoryImpl(
            mockJsonPlaceholderClient,
            mockPostDao,
            mockUserDao,
            mockAlbumDao,
            mockPhotoDao
        )
    }

    @Test
    fun `given empty database, when retrieve posts and users, then store them in db`() {
        runBlocking {
            whenever(mockPostDao.getPosts()).thenReturn(flowOf(listOf()))
            whenever(mockJsonPlaceholderClient.fetchPosts()).thenReturn(Result.Success(listOf()))
            whenever(mockJsonPlaceholderClient.fetchUsers()).thenReturn(Result.Success(listOf()))
            whenever(mockPostDao.insertPosts(listOf())).thenReturn(Unit)

            val test = repository.retrieveAndStorePostsAndUsers()

            verify(mockJsonPlaceholderClient).fetchPosts()
            Assert.assertEquals(test, Result.Success(Unit))
        }
    }
}
